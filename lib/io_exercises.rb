# I/O Exercises
#
# * Write a `guessing_game` method. The computer should choose a number between
#   1 and 100. Prompt the user to `guess a number`. Each time through a play loop,
#   get a guess from the user. Print the number guessed and whether it was `too
#   high` or `too low`. Track the number of guesses the player takes. When the
#   player guesses the number, print out what the number was and how many guesses
#   the player needed.
# * Write a program that prompts the user for a file name, reads that file,
#   shuffles the lines, and saves it to the file "{input_name}-shuffled.txt". You
#   could create a random number using the Random class, or you could use the
#   `shuffle` method in array.
def guessing_game
  number = Random.rand(1..100)
  guess = nil
  num_guesses = 0

  until guess == number
    print "Guess a number: "
    guess = gets.chomp.to_i
    puts guess
    num_guesses += 1

    if guess != number
      puts guess > number ? "too high" : "too low"
    end
  end

  puts "Correct number: #{number}"
  puts "Number of guesses: #{num_guesses}"

end

def file_shuffler
  print "Provide file to be shuffled: "
  file_name = gets.chomp

  contents = File.readlines(file_name)
  contents.shuffle!

  File.open("#{file_name[0...-4]}-shuffled.txt", "w") do |f|
    contents.each { |line| f.puts line }
  end
end

if __FILE__ == $PROGRAM_NAME
  
end
